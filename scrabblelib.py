import enum
import platform

VERSION = "1.0.1"
AUTHOR = "Caleb DeLaBruere"
ENCODING = 'ascii'


class MessageType(enum.Enum):
    HELLO = 1
    QUIT = 2
    OK = 3
    NOK = 4
    USERSET = 5
    USERCHANGE = 6
    USERJOIN = 7
    READY = 8
    STARTING = 9
    SCORE = 10
    BOARDPUSH = 11
    TILES = 12
    TURN = 13
    PLACE = 14
    PASS = 15
    EXCHANGE = 16
    WINNER = 17
    GOODBYE = 18


class Message:
    def __init__(self, msgtype, content=None):
        self.msgtype = msgtype
        self.content = content

class Player:
    def __init__(self, client=None, username="None"):
        self.client = client
        self.username = username
        self.score = 0
        self.tiles = ""
        self.numPasses = 0

class Board:
    def __init__(self, SIZE=15):
        self.SIZE = SIZE
        self.BOARD = self.initBoard()

    def initBoard(self):
        PREMIUM_CELLS = [[4, 0, 0, 1, 0, 0, 0, 4, 0, 0, 0, 1, 0, 0, 4],
                        [0, 3, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 3, 0],
                        [0, 0, 3, 0, 0, 0, 1, 0, 1, 0, 0, 0, 3, 0, 0],
                        [1, 0, 0, 3, 0, 0, 0, 1, 0, 0, 0, 3, 0, 0, 1],
                        [0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0],
                        [0, 2, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 2, 0],
                        [0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0],
                        [4, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 4],
                        [0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0],
                        [0, 2, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 2, 0],
                        [0, 0, 0, 0, 3, 0, 0, 0, 0, 0, 3, 0, 0, 0, 0],
                        [1, 0, 0, 3, 0, 0, 0, 1, 0, 0, 0, 3, 0, 0, 1],
                        [0, 0, 3, 0, 0, 0, 1, 0, 1, 0, 0, 0, 3, 0, 0],
                        [0, 3, 0, 0, 0, 2, 0, 0, 0, 2, 0, 0, 0, 3, 0],
                        [4, 0, 0, 1, 0, 0, 0, 4, 0, 0, 0, 1, 0, 0, 4]]
        
        lst = [[ self.BoardTile(0,0) for col in range(self.SIZE)] for row in range(self.SIZE)]
        for i in range(self.SIZE):
            for j in range(self.SIZE):
                lst[i][j].modifier = PREMIUM_CELLS[i][j]
        return lst

    def printBoard(self):
        for row in self.BOARD:
            print("")
            for tile in row:
                print("(", tile.letter, ",", tile.modifier, ") ", end =" ")

    def buildBoard(self):
        tmp = ""
        for row in self.BOARD:
            tmp = tmp + '\r\n'
            for tile in row:
                tmp = tmp + '(' + str(tile.letter) + ',' + str(tile.modifier) + ')'
        return tmp
        
    class BoardTile:
        def __init__(self, letter, modifier):
            self.letter = letter
            self.modifier = modifier

def buildMessage(msgtype, content=None):
    if content is not None:
        switcher = {
            MessageType.OK: "OK " + content + '\r\n',
            MessageType.NOK: "NOK " + content + '\r\n',
            MessageType.HELLO: "HELLO " + content + '\r\n',
            MessageType.USERSET: "USERSET " + content + '\r\n',
            MessageType.USERCHANGE: "USERCHANGE " + content + '\r\n',
            MessageType.USERJOIN: "USERJOIN " + content + '\r\n',
            MessageType.SCORE: "SCORE " + content + '\r\n',
            MessageType.BOARDPUSH: "BOARDPUSH" + '\r\n' + content + '\r\n\r\n',
            MessageType.TILES: "TILES " + content + '\r\n',
            MessageType.TURN: "TURN " + content + '\r\n',
            MessageType.PLACE: "PLACE " + content + '\r\n',
            MessageType.EXCHANGE: "EXCHANGE " + content + '\r\n',
            MessageType.WINNER: "WINNER " + content + '\r\n'
        }
    else:
        switcher = {
            MessageType.READY: "READY" + '\r\n',
            MessageType.QUIT: "QUIT" + '\r\n',
            MessageType.STARTING: "STARTING" + '\r\n',
            MessageType.GOODBYE: "GOODBYE" + '\r\n', 
            MessageType.OK: "OK"  + '\r\n',
            MessageType.EXCHANGE: "EXCHANGE" + '\r\n',
            MessageType.PASS: "PASS" + '\r\n'
        }

    return switcher.get(msgtype, "ERROR: Message Type Not Found").encode(ENCODING)



def decodeMessage(message):
    decoded = message.decode(ENCODING)
    splitMessages = set()
    boardpush=False
    board=''
    message = decoded.split("\r\n",1)
    while message: 
        #Cases for commands with no content
        if boardpush:
            board = board + str(message[0]) + "\n"
        else:
            if message[0].startswith("BOARDPUSH"):
                boardpush=True
                message = message[1].split("\r\n",1)
            elif message[0] == "READY":
                splitMessages.add(Message(MessageType.READY))
            elif message[0] == "STARTING":
                splitMessages.add(Message(MessageType.STARTING))
            elif message[0] == "OK":
                splitMessages.add(Message(MessageType.OK))
            elif message[0] == "QUIT":
                splitMessages.add(Message(MessageType.QUIT))
            elif message[0] == "GOODBYE":
                splitMessages.add(Message(MessageType.GOODBYE))
            elif message[0] == "PASS":
                splitMessages.add(Message(MessageType.PASS))
            #Cases for commands with content
            else:
                    messageData = message[0].split(" ", 1)
                    if messageData[0] == "HELLO":
                        splitMessages.add(Message(MessageType.HELLO, messageData[1]))
                    elif messageData[0] == "OK":
                        splitMessages.add(Message(MessageType.OK, messageData[1]))
                    elif messageData[0] == "NOK":
                        splitMessages.add(Message(MessageType.NOK, messageData[1]))
                    elif messageData[0] == "USERCHANGE":
                        splitMessages.add(Message(MessageType.USERCHANGE, messageData[1]))
                    elif messageData[0] == "USERSET":
                        splitMessages.add(Message(MessageType.USERSET, messageData[1]))
                    elif messageData[0] == "USERJOIN":
                        splitMessages.add(Message(MessageType.USERJOIN, messageData[1]))
                    elif messageData[0] == "SCORE":
                        splitMessages.add(Message(MessageType.SCORE, messageData[1]))
                    elif messageData[0] == "TILES":
                        splitMessages.add(Message(MessageType.TILES, messageData[1]) )  
                    elif messageData[0] == "TURN":
                        splitMessages.add(Message(MessageType.TURN, messageData[1]) )
                    elif messageData[0] == "PLACE":
                        splitMessages.add(Message(MessageType.PLACE, messageData[1]) )
                    elif messageData[0] == "WINNER":
                        splitMessages.add(Message(MessageType.WINNER, messageData[1]) )
                    elif messageData[0] == "EXCHANGE":
                        try:
                            if messageData[1].isalpha():
                                splitMessages.add(Message(MessageType.EXCHANGE, messageData[1]))
                            else:
                                splitMessages.add(Message(MessageType.EXCHANGE))
                        except IndexError:
                            splitMessages.add(Message(MessageType.EXCHANGE))
        try:
            if boardpush & message[1].startswith("\r\n"):
                splitMessages.add(Message(MessageType.BOARDPUSH, board))
                boardpush=False
            message = message[1].split("\r\n",1)
        except IndexError:
            message = ''
    return(splitMessages)

        
        
