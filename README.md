# Scrabble

Edit: There is a hardcoded path to /etc/dictionaries-common/words as well as a hardcoded path to my home directory [here](https://bitbucket.org/delabrcd/scrabble/src/033ee8022a10442056361eacc2f6802ae041f431/scrabblegamelib.py#lines-44). I'll get around to fixing those later.

Run: 

`git clone https://bitbucket.org/delabrcd/scrabble.git`

The scrabble repository contains a server and client solution that implement the scrabble protocol as found [here](https://people.clarkson.edu/~bashawhm/SCRABBLE_RFC_v1_0_1.txt).  The Scrabble protocol is implemented using TCP and sends byte text streams over a stream socket. 

The scrabble server implements python's threading library and accepts connections from as many clients as it can before starting the game. 

scrabblelib.py and scrabblegamelib.py are required dependencies for both server and client to run.  These libraries respectively implement "encoding" and "decoding" of the scrabble socket protocol and assist the server in playing the game



**To run server:**

`python3 scrabble_server.py <port_num>`

**To run client:**

`python3 scrabble_client.py <server_IP> <server_port>`