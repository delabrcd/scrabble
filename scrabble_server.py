import socket 
from _thread import *
import threading 
from threading import Thread

import platform
import sys
import queue
import time
import random

import scrabblelib
import scrabblegamelib

QUIT_LOCK = threading.Lock()
SOCKET_LOCK = threading.Lock()
TURN_LOCK = threading.Lock()



VERSION = "1.0.1"
AUTHOR = "Caleb DeLaBruere"
NUM_PLAYERS = 1
NUM_READY = 0
PLAYING = False
PLAYERS = dict()
host = "" 
port = 9000
CURRENT_TURN = ""



def newClient(client, addr, broadcastQueue, readyQueue, quitQueue, turnFinishQueue,turnStartQueue,tilesQueue): 
    global PLAYERS
    username = str(addr[0])
    score = 0
    myTiles = ""
    try:
        helloHandshake(client, addr)
    except ValueError:
        return 1
    broadcastQueue.put(scrabblelib.buildMessage(scrabblelib.MessageType.USERJOIN, username))
    newPlayers = True
    while newPlayers:
        rawmsg = client.recv(1024)
        if not rawmsg:
            print(username + " Disconnected")
            break
        msgs = scrabblelib.decodeMessage(rawmsg)
        for msg in msgs:
            if msg.msgtype == scrabblelib.MessageType.USERSET:
                response = scrabblelib.buildMessage(scrabblelib.MessageType.USERCHANGE, username + " " + msg.content)
                username = msg.content
                broadcastQueue.put(response)
                QUIT_LOCK.acquire()
                PLAYERS[client].username = username
                QUIT_LOCK.release()
            elif msg.msgtype == scrabblelib.MessageType.READY:
                readyQueue.put(1)
                response = scrabblelib.buildMessage(scrabblelib.MessageType.OK, "Game will start when everyone is ready")
                client.send(response)
            elif msg.msgtype == scrabblelib.MessageType.QUIT:
                quit(client)
                newPlayers = False
            else:
                client.send(scrabblelib.buildMessage(scrabblelib.MessageType.NOK, "C'mon, wait your turn"))
    return 0

def helloHandshake(client, addr):
    helloContent = VERSION + ", " + platform.platform() + ",Python," + AUTHOR
    client.send(scrabblelib.buildMessage(scrabblelib.MessageType.HELLO, helloContent))
    rawmsg = client.recv(1024)
    msgs = scrabblelib.decodeMessage(rawmsg)
    for msg in msgs:
        if msg.msgtype == scrabblelib.MessageType.HELLO:
            data = msg.content.split(',')
            clientVersion = data[0]
            if VERSION == clientVersion:
                client.send(scrabblelib.buildMessage(scrabblelib.MessageType.OK, "Please send username, otherwise you will be known as " + addr[0]))
                print('Connected to:', addr[0], ':', addr[1]) 
            else:
                quit(client)
                raise ValueError('Versions do not match')
        else: 
            quit(client)
            raise ValueError('HELLO Command not sent first')

def broadcastThread(broadcastQueue):
    
    global PLAYERS
    remove = set()
    while True:
        msg = broadcastQueue.get()
        QUIT_LOCK.acquire()
        for player in PLAYERS.values():   
            try: 
                player.client.sendall(msg)
            except BrokenPipeError:
                pass
        QUIT_LOCK.release()

def readyThread(readyQueue, s, broadcastQueue):
    
    global NUM_READY
    global PLAYING
    global PLAYERS
    while True:
        num = readyQueue.get()
        NUM_READY += num
        okMsg = str(NUM_READY) + " / " + str(NUM_PLAYERS) + " Players Are Ready"
        broadcastQueue.put(scrabblelib.buildMessage(scrabblelib.MessageType.OK, okMsg))
        if NUM_PLAYERS == NUM_READY:   
            PLAYING = True  
            tmp = socket.socket(socket.AF_INET, 
                  socket.SOCK_STREAM)
            tmp.connect((host,port))
            tmp.send(scrabblelib.buildMessage(scrabblelib.MessageType.QUIT))
            tmp.close
            return 0
            
def quit(client):
    global PLAYERS
    global NUM_PLAYERS
    global PLAYING
    NUM_PLAYERS -= 1
    try:
        client.send(scrabblelib.buildMessage(scrabblelib.MessageType.GOODBYE))
    except:
        pass

    QUIT_LOCK.acquire()
    PLAYERS.pop(client)
    QUIT_LOCK.release()
    client.close()     

def turnManage(turnFinishQueue,turnStartQueue,broadcastQueue,tilesQueue,players):
    global CURRENT_TURN
    global PLAYING
    bag = scrabblegamelib.get_full_bag()
    board = scrabblelib.Board(15)
    for player in players.values():
        player.client.sendall(scrabblelib.buildMessage(scrabblelib.MessageType.SCORE, str(player.score) + " " + str(player.username)))
        player.tiles = scrabblegamelib.generate_rack(player.tiles, bag)
        player.client.sendall(scrabblelib.buildMessage(scrabblelib.MessageType.TILES, player.tiles))

    while PLAYING:
        for player in (list(players.values())):
            try:
                broadcastQueue.put(scrabblelib.buildMessage(scrabblelib.MessageType.TURN, player.username))
                broadcastQueue.put(scrabblelib.buildMessage(scrabblelib.MessageType.BOARDPUSH, board.buildBoard()))
                broadcastQueue.put(scrabblelib.buildMessage(scrabblelib.MessageType.SCORE, str(player.score) + " " + str(player.username)))
                player.tiles = scrabblegamelib.generate_rack(player.tiles, bag)
                player.client.sendall(scrabblelib.buildMessage(scrabblelib.MessageType.TILES, player.tiles))
                rawmsg = player.client.recv(2000)
                msgs = scrabblelib.decodeMessage(rawmsg)
                for msg in msgs:
                    if msg.msgtype == scrabblelib.MessageType.PLACE:
                        placeTiles = msg.content.split()
                        lastIndex = []
                        for tile in placeTiles:
                            index = tile.replace("(","")
                            index = index.replace(")","").split(',')

                            if (index[0].upper() in player.tiles) and (board.BOARD[int(index[1])][int(index[2])].letter == 0):
                                if isAdjacent(index, board.BOARD):
                                    player.tiles = player.tiles.replace(index[0].upper(), "",1)
                                    board.BOARD[int(index[1])][int(index[2])].letter = index[0].upper()
                                    lastIndex = [int(index[1]), int(index[2])]
                                    player.client.send(scrabblelib.buildMessage(scrabblelib.MessageType.OK, "Successful Move " + tile))
                                    
                                else:
                                    player.client.send(scrabblelib.buildMessage(scrabblelib.MessageType.NOK, "Illegal Move " + tile))
                                
                            else:
                                player.client.send(scrabblelib.buildMessage(scrabblelib.MessageType.NOK, "Illegal Move " + tile))
                        words = getWords(False, board.BOARD, True, placeTiles)
                        for score in words:
                            player.score = player.score + words[score] 
                    elif msg.msgtype == scrabblelib.MessageType.EXCHANGE:
                        if msg.content is None:
                            for tile in player.tiles:
                                bag += tile
                            player.tiles = ""
                            player.tiles = scrabblegamelib.generate_rack(player.tiles, bag)
                        else:
                            bag += msg.content.upper()                            
                            player.tiles = player.tiles.replace(msg.content.upper(), "",1)
                            player.tiles = scrabblegamelib.generate_rack(player.tiles, bag)
                    elif msg.msgtype == scrabblelib.MessageType.PASS:
                        PLAYERS[player.client].numPasses = PLAYERS[player.client].numPasses+1
                        if PLAYERS[player.client].numPasses > 1:
                            PLAYING = False
                    elif msg.msgtype == scrabblelib.MessageType.QUIT:
                        quit(player.client)   
                        PLAYING = False
                    else:
                        player.client.send(scrabblelib.buildMessage(scrabblelib.MessageType.NOK, "Command Not Recognized"))
                broadcastQueue.put(scrabblelib.buildMessage(scrabblelib.MessageType.SCORE, str(PLAYERS[player.client].score) + " " + str(player.username)))
            except:
                 pass

def isAdjacent(tile, board):
    adjacent = False
    if (int(tile[1]) == 7 and int(tile[2]) == 7) and board[int(tile[1])+1][int(tile[2])].letter == 0:
        adjacent = True
    if int(tile[1])+1 < 15:
        if not board[int(tile[1])+1][int(tile[2])].letter == 0:
            adjacent = True
    if int(tile[1])-1 > -1:
        if not board[int(tile[1])-1][int(tile[2])].letter == 0:
            adjacent = True
    if int(tile[2])+1 < 15:
        if not board[int(tile[1])][int(tile[2])+1].letter == 0:
            adjacent = True
    if int(tile[2])-1 > -1:
        if not board[int(tile[1])][int(tile[2])-1].letter == 0:
            adjacent = True     
    return adjacent


def getWords(flipXY, board, firstCall, content):
    finalWords = dict()
    for tile in content:
        word = ""
        score = 0
        mod = 1
        
        index = tile.replace("(","")
        index = index.replace(")","").split(',')
        if flipXY:
            tmp = index[1]
            index[1] = index[2]
            index[2] = tmp
        firstIndex = int(index[1])
        for i in range(int(index[1]), -1, -1):
            if not board[i][int(index[2])].letter == 0:
                firstIndex = i
            else: break
        for i in range(firstIndex, 15, 1):
            if not board[i][int(index[2])].letter == 0:
                word += board[i][int(index[2])].letter
                value = scrabblegamelib.LETTER_VALUES.get(board[i][int(index[2])].letter)
                if not board[i][int(index[2])].modifier == 0:
                    if board[i][int(index[2])].modifier == 1:
                        value = value * 2
                    elif board[i][int(index[2])].modifier == 2:
                        value = value * 3
                    elif board[i][int(index[2])].modifier == 3:
                        mod = mod + 1
                    elif board[i][int(index[2])].modifier == 4:
                        mod = mod + 2
                score = score + value
            else:
                print("Found: ", word)
                if len(word) > 1 & scrabblegamelib.check_word(word):
                    finalWords[word] = score * mod
                break
        if firstCall:
            rez = [[board[x][y] for x in range(len(board))] for y in range(len(board[0]))]  
            if int(index[2]) != 14:
                if not board[int(index[1])][int(index[2])+1].letter == 0:
                    finalWords.update(getWords(True, rez, False, content))
            if int(index[2]) != 0:
                if not board[int(index[1])][int(index[2])-1].letter == 0:
                    finalWords.update(getWords(True, rez, False, content))
    return finalWords

def findWinner(players):
    winner = scrabblelib.Player()
    for player in list(players.values()):
        if player.score > winner.score:
            winner = player
    return winner

def Main(): 
        global NUM_PLAYERS
        global PLAYERS
        global port
        try:
            if len(sys.argv) > 1:
                port = int(sys.argv[1])

            broadcastQueue = queue.Queue()
            readyQueue = queue.Queue()
            quitQueue = queue.Queue()
            turnFinishQueue = queue.Queue()
            turnStartQueue = queue.Queue()
            tilesQueue = queue.Queue()

            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            s.bind((host, port)) 
            print("Binded to port", port) 
        
            s.listen(5) 
            start_new_thread(broadcastThread, (broadcastQueue,))
            
            print("Listening...")   
            
            #Initial Player 
            client, addr = s.accept() 
            lastThread = ""
            lastThread = Thread(target=newClient, args=(client,addr,broadcastQueue, readyQueue, quitQueue, turnFinishQueue, turnStartQueue,tilesQueue))
            lastThread.start()        
            QUIT_LOCK.acquire(blocking=False)
            PLAYERS[client] = scrabblelib.Player(client)
            QUIT_LOCK.release()
            start_new_thread(readyThread, (readyQueue, s, broadcastQueue))
            client = ""
            
            #New players
            while not PLAYING: 
                client, addr = s.accept() 
                NUM_PLAYERS+=1
                lastThread = Thread(target=newClient, args=(client,addr,broadcastQueue, readyQueue, quitQueue, turnFinishQueue, turnStartQueue, tilesQueue))
                lastThread.start()
                QUIT_LOCK.acquire(blocking=False)
                PLAYERS[client] = scrabblelib.Player(client)
                QUIT_LOCK.release()

            lastThread.join()   
            broadcastQueue.put(scrabblelib.buildMessage(scrabblelib.MessageType.STARTING)) 
            playingThread =  Thread(target=turnManage, args=(turnFinishQueue, turnStartQueue,broadcastQueue,tilesQueue,PLAYERS))  
            playingThread.start()
            while(PLAYING):
                time.sleep(1)
            if NUM_PLAYERS > 0:
                winner = findWinner(PLAYERS)
                broadcastQueue.put(scrabblelib.buildMessage(scrabblelib.MessageType.WINNER, str(winner.score) + " " + winner.username))
            sys.exit()
        except KeyboardInterrupt:
            sys.exit()

        
        
  
if __name__ == '__main__': 
    Main() 

