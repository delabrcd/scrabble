import random
import string

LETTERS = """\
AAAAAAAAAB\
BCCDDDDEEE\
EEEEEEEEEF\
FGGGHHIIII\
IIIIIJKLLL\
LMMNNNNNNO\
OOOOOOOPPQ\
RRRRRRSSSS\
TTTTTTUUUU\
VVWWXYYZ\
"""

LETTER_VALUES = {"A": 1,
                 "B": 3,
                 "C": 3,
                 "D": 2,
                 "E": 1,
                 "F": 4,
                 "G": 2,
                 "H": 4,
                 "I": 1,
                 "J": 1,
                 "K": 5,
                 "L": 1,
                 "M": 3,
                 "N": 1,
                 "O": 1,
                 "P": 3,
                 "Q": 10,
                 "R": 1,
                 "S": 1,
                 "T": 1,
                 "U": 1,
                 "V": 4,
                 "W": 4,
                 "X": 8,
                 "Y": 4,
                 "Z": 10,}

f = open('/home/delabrcd/CS/Scrabble/words', 'r')
x = set()
for line in f:
    x.add(line.upper().replace("\n", ""))
f.close()

def get_full_bag():
    """Returns a list of letters in the whole bag."""
    LETTERS + random.choice(string.ascii_letters) + random.choice(string.ascii_letters)
    return list(LETTERS)

def generate_rack(rack, bag):

    # Put random letters at the front.
    random.shuffle(bag)

    # Figure out how many letters we need.
    needed_letters = 7 - len(rack)

    # Fill up the rack.
    rack += "".join(bag[:needed_letters])

    # Remove from the bag.
    del bag[:needed_letters]

    return rack

def check_word(word):
    return word in x

