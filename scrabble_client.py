import socket 
import platform
import enum
import time
import sys
import threading
from _thread import *
PRINT_LOCK = threading.Lock()
MYTURN_LOCK = threading.Lock()

import scrabblelib

VERSION = "1.0.1"
HOST = '127.0.0.1'
PORT = 12348
AUTHOR = "Caleb DeLaBruere"
ENCODING = 'ascii'
GAMESTART = False
MYTURN = False
TILES = ""
NAME = ''
PLAYING = True

def helloHandshake(s):
    rawmsg = s.recv(1024)
    if not rawmsg: 
        print("Server Closed") 
    msgs = scrabblelib.decodeMessage(rawmsg)
    for msg in msgs:
        if msg.msgtype == scrabblelib.MessageType.HELLO:
            data = msg.content.split(',')
            serverVersion = data[0]
            if VERSION == serverVersion:
                helloContent = VERSION + ", " + platform.platform() + ",Python," + AUTHOR
                s.send(scrabblelib.buildMessage(scrabblelib.MessageType.HELLO, helloContent))
            else:
                print("ERROR: Server version " + serverVersion + " and clientVersion " + VERSION + " do not mach")
                sys.exit
                return 1
        else:
            print("ERROR: Server did not send HELLO")
            sys.exit
            return 1
        
    print("Successfully Connected to: " + msg.content)
    rawmsg = s.recv(1024)
    if not rawmsg: 
        print("Server Closed")  
    msgs = scrabblelib.decodeMessage(rawmsg)
    for msg in msgs:
        print(msg.content)

def receiveMessages(s):
    global GAMESTART
    global MYTURN
    global TILES
    while True:

        rawmsg = s.recv(2000)
        msgs = scrabblelib.decodeMessage(rawmsg)
        for msg in msgs:
            if msg.msgtype == scrabblelib.MessageType.USERCHANGE:
                change = msg.content.split()
                print("\nServer: Changed user " + change[0] + " to " + change[1])
            elif msg.msgtype == scrabblelib.MessageType.USERJOIN:
                print("\nServer: " + msg.content.rstrip() + " Joined")
            elif msg.msgtype == scrabblelib.MessageType.OK:
                print(msg.content)
            elif msg.msgtype == scrabblelib.MessageType.NOK:
                print(msg.content)
            elif msg.msgtype == scrabblelib.MessageType.STARTING:
                print("Game Starting")
                GAMESTART = True
            elif msg.msgtype == scrabblelib.MessageType.BOARDPUSH:
                print("Board: \n" + msg.content)
            elif msg.msgtype == scrabblelib.MessageType.SCORE:
                print("Score:", msg.content)
            elif msg.msgtype == scrabblelib.MessageType.TILES:
                print("Your tiles: " + msg.content)
                TILES = msg.content
            elif msg.msgtype == scrabblelib.MessageType.TURN:
                print("It is", msg.content.rstrip() + "'s turn")
                if msg.content == NAME:
                    MYTURN = True
            elif msg.msgtype == scrabblelib.MessageType.WINNER:
                print("Winner is " + msg.content)
                s.send(scrabblelib.buildMessage(scrabblelib.MessageType.QUIT))
                s.close()
                MYTURN = False
                GAMESTART = False
                PLAYING = False
                sys.exit()
                return(0)

def readyUp(prompt, decisions):
    while True:
        resp = input(prompt + decisions).lower()
        if resp in ('y', 'yes'):
            return True
        if resp in ('q', 'quit', 'n', 'no'):

            return False
        print('Please enter ' + decisions)

def Main(): 
    global NAME
    global MYTURN
    global HOST
    global PORT
    global PLAYING
    if not len(sys.argv) > 2:
        print("usage:", sys.argv[0], "<host> <port>")
        sys.exit()
    else:
        HOST = sys.argv[1]
        PORT = int(sys.argv[2])
    try:
        
        s = socket.socket(socket.AF_INET,socket.SOCK_STREAM) 
        s.connect((HOST,PORT)) 
        helloHandshake(s)
        start_new_thread(receiveMessages, (s,))
        NAME = input("Username: ")
        s.send(scrabblelib.buildMessage(scrabblelib.MessageType.USERSET, NAME))
        ready = readyUp("Ready up? ", "[\'Y\' to ready up / \'Q\' to quit] ")
        if ready:
            s.send(scrabblelib.buildMessage(scrabblelib.MessageType.READY))
        else:
            s.send(scrabblelib.buildMessage(scrabblelib.MessageType.QUIT))
            s.close()
            return 0
        while PLAYING:
            time.sleep(1)
            if GAMESTART & MYTURN:
                 prompt = "Enter your move: "
                 decisions = "[\'P\' to place / \'E\' to exchange / \'N\' to pass] "
                 resp = input(prompt + decisions).lower()
                 if resp in ('p', 'place'):
                    place = ""
                    while MYTURN:
                        row = input("Enter Row:")
                        collumn = input("Enter Collumn: ")
                        letter = input("Enter Letter to Place:")
                        place+=("("+letter+','+row+','+collumn+") ")
                        ready = readyUp("Place another? ", "[\'Y\' to place / \'Q\' to quit] ")
                        if not ready:
                            MYTURN_LOCK.acquire()
                            MYTURN = False
                            MYTURN_LOCK.release()
                    print(place)
                    s.send(scrabblelib.buildMessage(scrabblelib.MessageType.PLACE, place))

                 elif resp in ('e', 'exchange'):
                    TRY = True
                    while TRY:
                        letter = input("Letter to Exchange: [Press Enter to Enchage all Tiles]")
                        if letter in TILES:
                            s.send(scrabblelib.buildMessage(scrabblelib.MessageType.EXCHANGE, letter))
                            TRY = False
                        elif resp in '':
                            s.send(scrabblelib.buildMessage(scrabblelib.MessageType.EXCHANGE))
                            TRY = False
                        else:
                            print("That's not your tile")
                 elif resp in ('n', 'pass'):
                    s.send(scrabblelib.buildMessage(scrabblelib.MessageType.PASS))
        return 0         
        # s.close() 
        
    except KeyboardInterrupt:
        if s:
            print("\nClosing Server")
            s.send(scrabblelib.buildMessage(scrabblelib.MessageType.QUIT))
            s.close()
            return 0
    return 0
  
if __name__ == '__main__': 
    Main() 
